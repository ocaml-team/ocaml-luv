Source: ocaml-luv
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Andy Li <andy@onthewings.net>
Build-Depends:
 debhelper-compat (= 13),
 ocaml,
 ocaml-dune (>= 2.0.0),
 libctypes-ocaml-dev (>= 0.14.0),
 libresult-ocaml-dev,
 libuv1-dev (>= 1.40.0),
 dh-ocaml (>= 1.2)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-luv
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-luv.git
Homepage: https://github.com/aantron/luv

Package: libluv-ocaml-dev
Architecture: any
Depends:
 libuv1-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: OCaml binding to libuv (Development package)
 Luv is a binding to libuv, the cross-platform C library that does
 asynchronous I/O in Node.js and runs its main loop.
 .
 Besides asynchronous I/O, libuv also supports multiprocessing and
 multithreading. Multiple event loops can be run in different
 threads. libuv also exposes a lot of other functionality, amounting
 to a full OS API, and an alternative to the standard module Unix.
 .
 This package contains the development library of Luv.

Package: libluv-unix-ocaml-dev
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: Helpers for interfacing OCaml Luv and Unix (Development package)
 Luv is a binding to libuv, the cross-platform C library that does
 asynchronous I/O in Node.js and runs its main loop.
 .
 Besides asynchronous I/O, libuv also supports multiprocessing and
 multithreading. Multiple event loops can be run in different
 threads. libuv also exposes a lot of other functionality, amounting
 to a full OS API, and an alternative to the standard module Unix.
 .
 This package contains the development library of luv_unix, helpers for
 interfacing Luv and Unix.

Package: libluv-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: OCaml binding to libuv (Runtime library)
 Luv is a binding to libuv, the cross-platform C library that does
 asynchronous I/O in Node.js and runs its main loop.
 .
 Besides asynchronous I/O, libuv also supports multiprocessing and
 multithreading. Multiple event loops can be run in different
 threads. libuv also exposes a lot of other functionality, amounting
 to a full OS API, and an alternative to the standard module Unix.
 .
 This package contains the runtime library of Luv.

Package: libluv-unix-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: Helpers for interfacing OCaml Luv and Unix (Runtime library)
 Luv is a binding to libuv, the cross-platform C library that does
 asynchronous I/O in Node.js and runs its main loop.
 .
 Besides asynchronous I/O, libuv also supports multiprocessing and
 multithreading. Multiple event loops can be run in different
 threads. libuv also exposes a lot of other functionality, amounting
 to a full OS API, and an alternative to the standard module Unix.
 .
 This package contains the runtime library of luv_unix, helpers for
 interfacing Luv and Unix.
